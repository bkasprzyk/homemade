#!/usr/bin/python3

'''
script used contains a function which
formarts data into report format
'''


def gen_report(data):

    def get_formatted_text(iterable, name="", **kwargs):

        '''
            Helper function for gen_report
        '''

        text = ""

        #if key cnt is in dict
        if kwargs.get('cnt', False):

            #if key 'cnt' is true or non empty
            if kwargs['cnt']:

                #display text for counter if cnt_text
                if kwargs.get('cnt_text', False):

                    #display counter text if cnt_text true or non empty
                    if kwargs['cnt_text']:

                        #if new_line
                        if kwargs.get("new_line", False):

                            #add new line to text
                            text += format(

                                '\n'
                            )

                        #add data to the text
                        text = format(

                            name + kwargs['cnt_text'] +
                            str(len(iterable)) + '\n\n' +
                            '\n'.join(str(i) for i in iterable) + '\n'
                        )
                else:

                    #add data to the text
                    text = format(

                        '\n' + name + str(len(iterable)) + '\n\n' +
                        '\n'.join(str(i) for i in iterable) +'\n'
                    )
        else:

            #add data to the text
             text = format(

                '\n' + name + '\n\n' +
                '\n'.join(str(i) for i in iterable) + '\n'
            )

        return text

    '''
        Doc string
    '''

    result = ''

    #if data is not a dict
    if type(data).__name__ != 'dict':

        return result

    #for dict in dicts
    for key, item in data.items():

        #if item is not a dict then jump to next iteration
        if not type(item).__name__ == 'dict':

            #jump to next
            continue

        #if keys are missing
        if (not item.get("text", False)
        or not item.get('data', False)):

            #jump to next
            continue

        #add text -> Header section to result
        result += format(

            "\n" + item['text']
        )

        #if key data is a list
        if type(item['data']).__name__ == 'list':

            #options (kwargs) for get_formatted_text
            options={

                'cnt' : item.get('cnt', False),
                'cnt_text' : item.get('cnt_text', False)
            }

            #concatenate the result
            result += get_formatted_text(

                item['data'], **options
            )

        #if key data is a dict
        elif type(item['data']).__name__ == 'dict':

            #add new line for readiness
            result += format(

                '\n'
            )

            #for key in dict (data)
            for _key, _item in item['data'].items():

                #options (kwargs) for get_formatted_text
                options={

                    'cnt' : item.get('cnt', False),
                    'cnt_text' : item.get('cnt_text', False),
                    'name' : format('\n' + _key),
                    'new_line' : True
                }

                #concatenate the result
                result += get_formatted_text(

                    _item, **options
                )

    return result

if __name__ == '__main__':

    test_dict = {
        'bla':
        {
            'text': 'Smieszne cosie ponizej:)',
            'data': [1, 2, 3, 4, 'piec', 'szesc'],
            'cnt': True,
            'cnt_text': ' Total: '
        },

        'bla2':
        {
            'text': 'Smieszne cosie ponizej i cos inne:)',
            'data': {
                'rysio': [1, 2, 3, 4, 5],
                'mysio': ['piec', 'osiem', 1]
            },

            'cnt': True,
            'cnt_text': ' Total: '
        }
    }

print(gen_report(test_dict))
