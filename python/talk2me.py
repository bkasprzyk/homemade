#!/usr/bin/python3

import sys, os


### functions ###

def get_user_input(**kwargs):

    answer = ""

    #checking if ask for input is in loop
    if kwargs.get("loop", False):

        #set defualt kwargs['message'] if empty
        if not kwargs['message']:

            kwargs['message'] = format(

                "Please enter the input (terminate input by typing EOF " +
                "and pressing enter)"
            )

        print(kwargs['message'])

        while "EOF" not in answer:

            if kwargs.get("secure", False):

                answer = getpass(

                    prompt = ''
                )

            else:

                answer = input()

        #remove EOF
        answer = answer.replace("EOF", "")

    else:

        #set defualt kwargs['message'] if empty
        if not kwargs.get('message', False):

            kwargs['message'] = format(

                "Please enter the input (terminate input by pressing enter"
            )

        #check if input should be secure
        if kwargs.get("secure", False):

            answer = getpass(

                prompt = kwargs['message']
            )

        else:

            answer = input(

                kwargs['message']
            )

        #while input is empty
        while not answer:

            #check if input should be secure
            if kwargs.get("secure", False):

                answer = getpass(

                    prompt = kwargs['message']
                )

            else:

                answer = input(

                    kwargs['message']
                )

    #check if answer should be converted
    if kwargs.get("convert", False):

        if kwargs['convert'].lower() == "int":

            try:

                tmp = answer
                answer = int(

                    answer
                )

            except:

                pass
                answer = tmp

        elif kwargs['convert'].lower() == "float":

            try:

                tmp = answer
                answer = float(

                    answer
                )

            except:

                pass
                answer = tmp

    return answer

def sayHello():

    return "Hi :)"

def saySomething():

    return "Blah blah"

def mirrorMe():

    return '3'

def letMeOut():

    return 'exiting !!'

if __name__ == '__main__':

    menu = {

        '1' : sayHello(),
        '2' : saySomething(),
        '3' : mirrorMe(),
        '4' : letMeOut()
    }

    menu_description = {

        '1' : 'print hello message',
        '2' : 'write some string',
        '3' : 'print 3',
        '4' : 'exit'
    }

    print("Menu: ")

    for key in menu.keys():

        print(key, menu_description[key])

    user_input = get_user_input(message = "Enter menu number:\n")

    while not user_input == '4':

        print(menu[user_input])

        print("Menu: ")

        for key in menu.keys():

            print(key, menu_description[key])

        user_input = get_user_input(message = "Enter menu number:\n")
