#!/usr/bin/python3

import xlsxwriter
import csv
import argparse
import sys
from ast import literal_eval

def data2Excel(source, destination, *args):

    #flag used to detemermine if everything went ok
    isOK = False

    #if source is string try load csv file (expected)
    if type(source).__name__== 'str':

        try:

            with open(source, 'r', encoding='utf_8') as csv_file:

                source = list(

                    csv.reader(csv_file)
                )

                #close csv file
                csv_file.close()

            #mark as good
            isOK = True

        except:

            pass

            #close csv file
            csv_file.close()

            print("Something went wrong while reading: ",
            source,
            os.exc_info())

            #mark as bad
            isOK = False

            return isOK

    ### importing data to excel ###

    try:

        #create workbook
        wb = xlsxwriter.Workbook(

            destination
        )

        #add worksheet
        ws = wb.add_worksheet()

        #row counter
        row = 0

        for record in source:

            #loop over field in a record line
            for column, field in enumerate(record, 0):

                tmp = field

                #if column index is in *args
                if column in args:

                    ### convert data to float ###

                    try:

                        field = float(

                            tmp
                        )

                    except:

                        pass
                        field = tmp

                ws.write(row, column, field)

            #increase row number
            row += 1

        # enable autofilter first: row, column; last: row, column
        ws.autofilter(0, 0, row, len(source[0])-1)

        #close workbook
        wb.close()

        #mark as good
        isOK = True

    except:

        pass

        #mark as bad
        isOK = False

        print("Failed to create: ",
        destination,
        "\n", sys.exc_info())

    return isOK

if __name__ == '__main__':

    ''' argparse '''

    parser = argparse.ArgumentParser()
    parser.add_argument('destinations', nargs='+',
        help='Full_path1 full_path2 ... full_path_N')

    ARGS=parser.parse_args()

    ''' destination extensions '''

    excel_extension = '.xlsx'
    csv_extension = '.csv'

    ''' looping over destinations '''

    for destination in ARGS.destinations:

        ''' importing /creating xslx destination '''

        data2Excel(destination, destination.replace(

            csv_extension, excel_extension)
        )
