#!/usr/bin/python3

from time import time, sleep
import cProfile


def waititng(N):

    print('I will wait for ', N, ' (s).')
    sleep(N)
    print('exiting')

def main():

    for i in range(0,4):

            waititng(i)

def run1(a):

    print("I am run1", a)

def run2(b):

    print("I am run2", b)

if __name__ == '__main__':

    cProfile.run('main()', '/tmp/test_perf.stats')

    '''
    pip download gprof2dot
    ./gprof2dot.py -f pstats /tmp/test_perf.stats |dot -Tpng -o output.png

    or run python3 -m cProfile python_script.py
    '''

    #run functions by names
    function_mapper = {

        'run1' :
        {
            'name' : run1('Matura is bzdura'),
        },

        'run2' :
        {
            'name' : run2('Alfa beta gamma'),
        }
    }

    for value in function_mapper.values():

        if value.get("name", False):

            value['name']
