#!/usr/bin/python3

import sys, os, argparse, sqlite3
from time import time, sleep
from copy import deepcopy
import re

### argparse ###

parser = argparse.ArgumentParser()

#dbs
parser.add_argument(

    'dbs',
     nargs = '+',
     help = 'db1 db2'
)

#or
parser.add_argument(

    '-or',
    '--or',
    dest = 'or',
    action='store_true',
    help = 'apply logical instead logical and (default) to fields'
)

#head
parser.add_argument(

    '-head',
    '--head',
    dest = 'head',
    type = int,
    help = 'print n first records'
)

#tail
parser.add_argument(

    '-tail',
    '--tail',
    dest = 'tail',
    type = int,
    help = 'print n last records'
)

#in_head
parser.add_argument(

    '-in_head',
    '--in_head',
    dest = 'in_head',
    type = int,
    help = 'search in n first records'
)

#in_tail
parser.add_argument(

    '-in_tail',
    '--in_tail',
    dest = 'in_tail',
    type = int,
    help = 'search in n last records'
)

#raw
parser.add_argument(

    '-raw',
    '--raw',
    dest = 'raw',
    action='store_true',
    help = 'print raw output'
)

#all
parser.add_argument(

    '-all',
    '--all',
    dest = 'all',
    action='store_true',
    help = 'print all records'
)

#json
parser.add_argument(

    '-json',
    '--json',
    dest = 'all',
    action='store_true',
    help = 'print in json format'
)

#case sensitive
parser.add_argument(

    '-case_sensitive',
    '--case_sensitive',
    dest = 'case_sensitive',
    action='store_true',
    help = 'case sensitive search'
)

#exact match
parser.add_argument(

    '-exact_match',
    '--exact_match',
    dest = 'exact_match',
    action='store_true',
    help = 'strictly compares fields'
)

#only match
parser.add_argument(

    '-only_match',
    '--only_match',
    dest = 'only_match',
    action='store_true',
    help = 'displays only matches'
)

#regexp
parser.add_argument(

    '-regexp',
    '--regexp',
    dest = 'regexp',
    action='store_true',
    help = 'treat provided value as regular expression'
)


#first matching
parser.add_argument(

    '-first',
    '--first',
    dest = 'first',
    action='store_true',
    help = 'print first matching'
)

#last matching
parser.add_argument(

    '-last',
    '--last',
    dest = 'last',
    action='store_true',
    help = 'print last matching'
)

#name
parser.add_argument(

    '-name',
    '--name',
    dest = 'name',
    nargs = '+',
    help = 'The first name of Author'
)

#surname
parser.add_argument(

    '-surname',
    '--surname',
    dest = 'surname',
    nargs = '+',
    help = 'The surname of Author'
)

#title
parser.add_argument(

    '-title',
    '--title',
    dest = 'title',
    nargs = '+',
    help = 'the book\'s tittle'
)

#year
parser.add_argument(

    '-year',
    '--year',
    dest = 'year',
    nargs = '+',
    help = 'year when book was written'
)


#parse args
ARGS = parser.parse_args()


### FUNCTIONS ###

def is_table(DBCONN, table_name):

    #if table exists
    exists = False

    try:

        DBCONN.cursor().execute(

            '''SELECT * FROM ''' + table_name
        )

        exists = True

    except:

        pass

    #return result
    return exists

def compare(search_value, *patterns, **kwargs):

    #store result of compare
    result = []

    regex_suffix, regex_prefix = '', ''

    if kwargs.get('strict', False):

        regex_prefix = r"" + "^"
        regex_suffix = "$"

    else:

        regex_prefix = r""
        regex_suffix =  ""

    ### check provided data ###
    if not patterns or not search_value:

        return result

    #if not regexp
    if not kwargs.get('regexp', False):

        patterns = [ re.escape(i) for i in patterns]

    #if not case sensitive
    if kwargs.get('case_sensitive', False):

        patterns = [ i.lower() for i in patterns]

        if type(search_value).__name__ == 'list':

            search_value = search_value.lower()

        elif type(search_value).__name__ == 'str':

            search_value = [i.lower() for i in search_value]

    #work based on type of search values
    if type(search_value).__name__ == 'list':

        result = [

            re.compile(regex_prefix + j + regex_suffix).findall(

                i
            ) for i in search_value for j in patterns
        ]

    else:

        result = [

            re.compile(regex_prefix + i + regex_suffix).findall(

                search_value
            ) for i in patterns
        ]

    ### eliminate empty string ###

    tmp = []

    for i in result:

        row = [j for j in i if j]

        if row:

            tmp.append(row)

    #get filtered result
    result = tmp

    return result

get_records(DBCONN, table_name, **kwargs):

    #if not head N or tail N then return all
    if not  kwargs.get('head', False) or kwargs.get('tail', False):

        kwargs['all'] = True

    #store all records
    records = []

    if is_table(DBCONN, table_name):

        for counter, record in enumerate(

            DBCONN.cursor().execute('''SELECT * FROM ''' + table_name
        ):

            records.append(

                record
            )

        #get N first from head
        if kwargs.get("head", False):

            records = records[:kwargs['head']]

        #get n first from tail
        elif kwargs.get("tail", False):

            records = records[kwargs['tail'] + 1 :]

    return records

def display_records(search_result, **kwargs):

    '''
    display_records(*args, **kwargs)

    Functions displays records

    '''

    message = ""

    #check search result
    if type(search_result).__name__ != 'list':

        return message

    elif not search_result:

        return message


    for result in search_result:

        if not ARGS.only_match:

            message = format(

                'name' + result[0] + ' Surname: ' + result[1] +
                ' tittle: ' + result[2] + ' Year: ' + result[3]
            )

        elif ARGS.raw:

            message = format(

                result[0] + ' ' + result[1] +
                ' ' + result[2] + ' ' + result[3]
            )

        elif ARGS.json:

            message = format(

            "it will be json"
        )



    #
    first match
    last match
    only match
    all
    raw
    head N
    last N
    json support




### Global options ###

OPTIONS = {

    'data_table_name' : 'myrecords',
    'data_base_name'  : 'example.db',

    'sql_query' : {

        'create_data_table'  : format(

            'CREATE TABLE myrecords ' +
            '(name text, surname text, title text, year real)'
        )
    }


}

if __name__ == '__main__':

    #parameters
    search_parameters = [

        {

            'name' : 'name',
            'value' : ARGS.name,
            'result' : [],
            'match' : [],
            'options' : {}
        },

        {

            'name' : 'surname',
            'value' : ARGS.surname,
            'result' : [],
            'match' : [],
            'options' : {}
        },

        {

            'name' : 'title',
            'value' : ARGS.title,
            'result' : [],
            'match' : [],
            'options' : {}
        },

        {

            'name' : 'year',
            'value' : ARGS.year,
            'result' : [],
            'match' : [],
            'options' : {}
        }
    ]

    ### add data to db ###
    for db in dbs:

        #if change to db was performed
        was_db_updated = False

        #create / open db
        DBCONNECT = sqlite3.connect(

            db,
            timeout = 30000
        )

        #enable wall
        DBCONNECT.execute('pragma journal_mode = wal')

        #create data table if not exists
        if not is_table(DBCONNECT, OPTIONS['data_table_name']):

            DBCONNECT.cursor().execute(

                OPTIONS['sql_query']['create_data_table']
            )

            #mark change
            was_db_updated = True

        #rows to add to db
        rows = [

            ('Alan','XYZ','BLa BLA', 1994),
            ('Bob','JSON','hi hi', 1995),
            ('Sean','Waka','oui oui', 1996)
        ]

        ### adding rows ###

        for row in rows:

            #adding single row
            DBCONNECT.cursor().execute(

                "INSERT INTO " + OPTIONS['data_table_name'] +
                " VALUES " + str(row)
            )

            sleep(30)

            #mark change
            was_db_updated = True

        #commit changes if required
        if was_db_updated:

            #commit changes
            DBCONNECT.commit()

    ### NORMAL PART ###

    for db in dbs:

        #Db conn
        DBCONNECT = sqlite3.connect(

            db,
            timeout = 30000
        )

        ### getting records based on requirement ###

        #store records
        records = []

        if (ARGS.all or not ARGS.head or not ARGS.tail
        or not ARGS.in_head or not ARGS.in_tail):

            records = get_records(

                DBCONNECT,
                OPTIONS['data_table_name']
            )

        elif ARGS.head or ARGS.in_head:

            records = get_records(

                DBCONNECT,
                OPTIONS['data_table_name'],
                head = ARGS.head if ARGS.head else ARGS.in_head
            )

        elif ARGS.tail or ARGS.in_tail:

            records = get_records(

                DBCONNECT,
                OPTIONS['data_table_name'],
                tail = ARGS.tail if ARGS.tail else ARGS.in_tail
            )

        #apply rules for every row
        for record in records:

            ### getting matches ###

            for parameter in search_parameters:

                #if value is empty for argument name
                if not parameter['value']:

                    continue

                #sotres match
                match = compare(

                    parameter['name'],
                    *parameter['value'],
                    ***parameters['options']
                )

                #if match found then add result and mark as found
                if match:

                    parameter['match'] = True
                    parameter['result'].append(match)

            ### checking the match ###

            #logical or
            if ARGS.or:

                for parameter in search_parameters:

                    if parameter['match'] and parameter['result']:

                        display_records()

                        #exit loop on first found
                        break

            #logical and
            else:

                #how many provided
                non_empty = [

                    True \
                    for i in search_parameters if search_parameters['value']

                ].count(

                    True
                )

                #how many matches
                found_match = [

                    True \
                    for i in search_parameters if search_parameters['result']

                ].count(

                    True
                )

                if non_empty == 4 and found_match == 4:

                    display_records()

                elif non_empty == 3 and found_match == 3:

                    display_records()

                elif non_empty == 2 and found_match == 2:

                    display_records()

                elif non_empty == 1 and found_match == 1:

                    display_records()
                if non_empty == 4 and found_match == 4:

                    display_records()

                elif non_empty == 3 and found_match == 3:

                    display_records()

                elif non_empty == 2 and found_match == 2:

                    display_records()

                elif non_empty == 1 and found_match == 1:

                    display_records()


            ### clear result ###

            for paramater in search_parameters

                parameter['result'].clear()

                parameter['match'].clear()



        """
        left to implement fix
        display_records is not implemented
        implement/improve displaying based on requirement
        fix  / correct first and last
        """
