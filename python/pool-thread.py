#!/usr/bin/python3

from time import sleep
from multiprocessing.dummy import Pool as ThreadPool

def fun1(timeout,message):

    print(message)
    sleep(timeout)
    return timeout,message

def fun2(timeout,message):

    print(message)
    sleep(timeout)


def fun3(timeout,message):

    print(message)
    sleep(timeout)


def fun4(timeout,message):

    print(message)
    sleep(timeout)

def fun5(arg):
    return "aa","bb"

if __name__ == '__main__':

    args={
        'fun1' : [[1,'ruuning fun1, waiting 1 sec'],
            [2,'running fun1, waiting 2 sec'],
            [3,'running fun1, waiting 3 sec'],
            [4,'running fun1, waiting 4 sec']],
        'fun2' : [[2,'running fun2, waiting 2 sec']],
        'fun3' : [[3,'running fun3, waiting 3 sec']],
        'fun4' : [[3,'running fun4, waiting 4 sec']]
    }

    '''
    for i in range(10):

        pool=ThreadPool()

        print("I am running this for ",i," time\n")

        t1=pool.starmap_async(fun1, args['fun1'])
        t2=pool.starmap_async(fun2, args['fun2'])
        t3=pool.starmap_async(fun3, args['fun3'])
        t4=pool.starmap_async(fun4, args['fun4'])
''
    for i in range(10):

        pool=ThreadPool()

        print("I am running this for ",i," time\n")

        t1=pool.starmap_async(fun1, args['fun1'])
        t2=pool.starmap_async(fun2, args['fun2'])
        t3=pool.starmap_async(fun3, args['fun3'])
        t4=pool.starmap_async(fun4, args['fun4'])

        t1.get()
        t2.get()
        t3.get()
        t4.get()
        pool.close()
        pool.join()

        t1.get()
        t2.get()
        t3.get()
        t4.get()
        pool.close()
        pool.join()
    '''

    pool=ThreadPool()

    t1=pool.starmap_async(fun1, args['fun1'])
    result = {i[0]:i[1] for i in t1.get()}

    dd={fun5(i)[0]:fun5(i)[1] for i in range(5)}
    print(dd,"\n",result)

    pool.close()
    pool.join()
