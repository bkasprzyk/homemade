#!/usr/bin/python3
import threading
from time import sleep


def fun(data, keyword, result, key):

    tmp=[]

    for i in data:

        if keyword.lower() == i.lower():

            tmp.append(i)
    sleep(5)

    result[key]=tmp




if  __name__ == '__main__':

    data_set=['alfa','beta','gamma','alfa']

    result={}

    config={

        'arg1' :
        {
            'args' : (data_set, 'beta', result, 'arg1', )
        },

        'arg2' :
        {
            'args' : (data_set, 'alfa', result, 'arg2', )
        }
    }


    thread1=threading.Thread(target=fun,args=config['arg1']['args'])
    thread2=threading.Thread(target=fun,args=config['arg2']['args'])

    ''' starting threads '''

    thread1.start()
    thread2.start()

    print("Currenlty running no of threads: ",threading.active_count())


    ''' waiting for threads to finish '''

    thread1.join()
    thread2.join()

    ''' printing results '''

    print("Result of thread1: ",result['arg1'],
    "\nResult of thread2: ", result['arg2'])
