#!/usr/bin/python3

import traceback
from datetime import datetime
import logging
import argparse

''' config logging '''
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s \
- %(message)s ',filename='/tmp/sypie.txt')

def gimmeTrue(data):

    tmp=data.copy()

    for iter, value in enumerate(data, 0):

        print("NO: ",iter," Value: ",value)

        if iter == 3:

            data[iter]=5000

    print("\n\n---------\n\n")

    return (tmp == data)

if __name__ == '__main__':

    ''' argparse '''

    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
        help='be vrbose', dest='v')

    ARGS=parser.parse_args()

    ''' Disbale verbose output if flag -v is NOT choosen '''

    if not ARGS.v:

        logging.disable(logging.DEBUG)


    ''' excepted True see function name :) '''

    i_want_true=True

    #will pass
    assert i_want_true == gimmeTrue([0,1,2]), gimmeTrue([0,1])

    #will fail
    #assert i_want_true == gimmeTrue([0,1,2,4,5]), gimmeTrue([0,1])

    ''' let's make an exception :) '''
    try:

        raise Exception("Yeah\n Houston we have a problem :)")

    except:

        error=open('/tmp/error.txt','w')
        error.write(str(datetime.now())+"\n"+traceback.format_exc())
        error.close()
        print("Error has occured, please inspect the file: ",
        "/tmp/error.txt")

    #debug
    logging.debug("To se logne\n")

    #critical
    logging.critical("No to syplo\n")
