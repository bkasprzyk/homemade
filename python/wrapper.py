#!/usr/bin/python3

import sys, argparse, subprocess

#argparse

parser = argparse.ArgumentParser()

parser.add_argument(

    '-menu_number',
    dest = 'menu_number',
    help='choose menu number',
    choices = ['1', '2', '3', '4'],
    type = str
)

ARGS = parser.parse_args()

def run_cmd(ip, cmd, **kwargs):

    result={
        'ip' : ip,
        'cmd': cmd,
        'output' : '',
        'status' : ''
    }

    #command
    command = ""

    ''' setting command base on kwargs '''

    if kwargs.get("local", False):

            command=format(cmd)
    else:

        if kwargs.get("command_switch", False):

            cmd=format(
            kwargs['command_switch']+" "+cmd
            )

        if kwargs.get("tool", False):

            command=format(
            kwargs['tool']+" "+ip +" "+cmd+" "
            )

            if kwargs.get("reverse", False):

                command=format(
                kwargs['tool']+" "+cmd+" "+ip
                )
        else:

            command=format(
            cmd+" "+ip
            )
    try:

        ''' running cmd '''

        result['output']=subprocess.check_output(command,
        universal_newlines=True,
        shell=True,
        stderr=subprocess.STDOUT
        )

        #setting status to fine
        result['status']='ok'

    except subprocess.CalledProcessError as exc:

        pass

        print("Running cmd: ",exc.cmd,
        "has finished with exit status: ",exc.returncode,
        "\nError: ",exc.output)

        #setting status to failed
        result['status']='failed'

        #clearing result
        result['output']=''

    return result


if __name__ == '__main__':

    send_me = format(

        '<<EOF' +
        '\n1' +
        "\n" +
        "3" +
        "\n" +
        "4" +
        "\n" +
        "EOF" +
        "\n"
    )

    script = '/home/smieszek/Dokumenty/skrypty/python/talk2me.py ' + send_me

    get_hello = run_cmd(

        "",
        script,
        local = True
    )

    print(get_hello['output'])

    send_me = "(sleep 2; echo 1 ; sleep 2; echo 3 ;sleep 2; echo 4) |"

    script = '/home/smieszek/Dokumenty/skrypty/python/talk2me.py '

    get_hello = run_cmd(

        "",
        send_me + script,
        local = True
    )

    print("Trying once again", get_hello['output'])
