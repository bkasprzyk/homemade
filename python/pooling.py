import multiprocessing
import time


def myfun(a,b,c):
    time.sleep(0.1)
    return {'val':(a*b*c) ** 10000}
def myfun2(a,b,c):
    time.sleep(0.1)
    return {'val':(a*b*c) ** 10000}

if __name__ == '__main__':

    multiprocessing.set_start_method("spawn")
    data=[]
    #st=time.time()
    for i in range(1000):

        #myfun(i,i,i)
        data.append((i,i,i))
    #print("elapsed",time.time()-st)

    st=time.time()
    pooling=multiprocessing.Pool()
    dd=pooling.starmap(myfun,data)

    pooling1=multiprocessing.Pool()
    dd=pooling1.starmap(myfun2,data)
    pooling1.close()
    pooling1.join()
    pooling.close()
    pooling.join()


    print("elapsed",time.time()-st)

    print(type(dd),dd[0])
