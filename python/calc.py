#!/usr/bin/python3

from collections import OrderedDict

import math

class Calc:


    def __init__(self):

        self.__score=0

    def __len__(self):

        return len(self.__score)

    def add(self, number):

        self.__score+=number

    def multiple(self, number):

        self.__score*=number

    def __str__(self):

        return "Sum: "+str(self.__score)


a=Calc()
a.add(5)
a.multiple(5)
print(a)

class Szukaczka:

    def __init__(self):

        self.__result=OrderedDict()

    def __len__(self):

        return len(self.__result)

    def __getitem__(self, position):

        return self.__result[position]

    def search(self, text, pattern):

        if type(text).__name__ == 'str':

            self.__result=OrderedDict({i:text.count(i) for i in text.split() })

        elif type(text).__name__ == 'list':

            self.__result=OrderedDict({j:i.count(j) for i in text \
            for j in i.split()})

        return self.__result

    def occurence(self, text):

        if type(text).__name__ == 'str':

            self.__result=OrderedDict({i:text.count(i) for i in text.split() })

        elif type(text).__name__ == 'list':

            self.__result=OrderedDict({j:i.count(j) for i in text \
            for j in i.split()})

        return self.__result

    def __str__(self):

        result=""

        for key in self.keys():

            result+=format("Word: "+key+" has "
            +self.__result[key]," occurences\n")

        return result

szuk=Szukaczka()
print(szuk.occurence("ala ma kota a kota ma rysia"))

'''
for i in ["ala ma kota","zenek zenek zenk","malpia sprawa"]:

    print(szuk.occurence())
'''
#print(szuk.occurence(["ala ma kota","zenek zenek zenk","malpia sprawa"]))
def fun(**kwargs):

    print(type(kwargs),kwargs)

def wr(**kwargs):

        fun(**kwargs)

wr(zwierzak=['kotek'])
