#!/usr/bin/python3

from time import time
from string import Template
import sys, os, argparse
import xml.etree.ElementTree as ET
import subprocess
from getpass import getpass

''' global varables neccessary functions '''

def isPath(path):

    if os.path.exists(path):

        return path
    else:

        message=format(
        "\nPath: "+path
        +" doesn't exist !!!!"
        )

        raise argparse.ArgumentTypeError(message)

''' argparse '''

parser = argparse.ArgumentParser()
parser.add_argument('ips', nargs = '*', help = 'IP1 IP2 .. IPN')
parser.add_argument('--template_dir', dest='template_dir',
    help='Full path to dir with templates', type = isPath, required = True)
parser.add_argument('-file', '--filename', dest = 'file',
    help='Full path to file with IP adresses, separator is new line/',
    type=isPath)

ARGS=parser.parse_args()

''' Global OPTIONS['get_templates'] '''

OPTIONS={

    'get_templates':
    {
        'file_extension' : '.xml',
        'template_number' : 'number',
        'template_name' : 'name',
        'multi_keys' : ['name', 'number', 'text', 'hidden',
        'name', 'number', 'text', 'hidden'],
        'multi_element_keys' : ['inputs', 'commands',
        'variables'],
        'shadows_name' : 'shadows',
        'shadow_name' : 'name'
    }
}

def get_templates(template_dir, dict_key = "name"):

    #result
    result={}

    #check type of template_dir
    if type(template_dir).__name__ == 'str':

        if not template_dir.endswith('/'):

            template_dir += '/'
    else:

        #if type is not str then return
        return result

    #get files from template_dir
    try:

        #filenames (absolute path)
        filenames = [

            template_dir + file \
            for file in os.listdir(template_dir) \
            if file.endswith(OPTIONS['get_templates']['file_extension'])
        ]

    except:

        pass

        print("Could not list files in dir: ", template_dir,
        "\n",sys.exc_info(), "\nStopping here!")

        return result

    #loop over file names  and load template
    for filename in filenames:

        try:

            #load xml file
            root = ET.parse(filename).getroot()

            #init template_key used for main dict key
            template_key = ""

            #### load multi keys ####

            for key in OPTIONS['get_templates']['multi_keys']:

                #find key in xml
                find_key = root.find(

                    key
                )

                #if key has text attribute
                if hasattr(find_key, 'text'):

                    #if element is template_number
                    if key == OPTIONS['get_templates']['template_number']:

                        try:

                            #convert to float
                            tmp = float(

                                find_key.text
                            )

                        except:

                            pass

                            #take uncoverted value
                            tmp = find_key.text

                        #if dict key is equal to key and no template key
                        if dict_key == key and not template_key:

                            #assign key value
                            template_key = tmp

                            #create a dict with under template_key
                            result[template_key] = {

                                key : template_key
                            }

                        else:

                            #if template key is defined
                            if template_key:

                                #add template number
                                result[template_key][key] = tmp
                    else:

                        #if dict key is equal to key and no template key
                        if dict_key == key and not template_key:

                            #assign key value
                            template_key = find_key.text

                            #create a dict with under template_key
                            result[template_key] = {

                                key : template_key
                            }

                        else:

                            #if template key is defined
                            if template_key:

                                #add key value
                                result[template_key][key] = find_key.text

            ### if template key not found ###

            if not template_key:

                print("Template key was not found",
                "\nPlease check the xml file: \n", filename,
                "\nJumping to next iteration.")

                #jump to next
                continue

            #### load multi element keys ###

            for key in OPTIONS['get_templates'].get(
            'multi_element_keys', ''):

                #if element are available in template under key
                template_variables = root.find(

                    key
                )

                #if any element name as OPTIONS['get_templates'][key]
                if template_variables:

                    #create empty dict with name
                    result[template_key][key] = {}

                    for variable in template_variables:

                        #if variable has attribue text and attrib
                        if (variable.attrib and variable.text
                        and hasattr(variable, 'attrib')
                        and (variable, 'text')):

                            #add variable name with value to dict
                            result[template_key][key]\
                            [variable.attrib['name']] = variable.text

            ### load shadows ###

            #find required key named OPTIONS['get_templates']['shadows_name]
            shadows = root.find(

                OPTIONS['get_templates']['shadows_name']
            )

            #if shadows are missing jump to next
            if not shadows:

                #jump to next
                continue

            #loop over shadow in shadows
            for shadow in shadows:

                # if element has a key name
                shadow_name = shadow.find(

                    OPTIONS['get_templates']['shadow_name']
                )

                #if shadow_name has no text attribute -> jump to next
                if not hasattr(shadow_name, 'text'):

                    #jump to next
                    continue

                #create shadows dict as shadows : { }
                if not result[template_key].get(
                OPTIONS['get_templates']['shadows_name'], False):

                    result[template_key]\
                    [OPTIONS['get_templates']['shadows_name']] = {}

                #create dict template_name :{}
                result[template_key]\
                [OPTIONS['get_templates']['shadows_name']]\
                [shadow_name.text] = {}

                ### load elements from shadow ###
                for element in shadow:

                    #load element tag : element text in shadow_name.text
                    result[template_key]\
                    [OPTIONS['get_templates']['shadows_name']]\
                    [shadow_name.text][element.tag] = element.text

        except:

            pass
            print("Could load template from file: ",
            filename,"\n",sys.exc_info())

    return result

def get_user_input(**kwargs):

    answer = ""

    #checking if ask for input is in loop
    if kwargs.get("loop", False):

        #set defualt kwargs['message'] if empty
        if not kwargs['message']:

            kwargs['message'] = format(

                "Please enter the input (terminate input by typing EOF " +
                "and pressing enter)"
            )

        print(kwargs['message'])

        while "EOF" not in answer:

            if kwargs.get("secure", False):

                answer = getpass(

                    prompt = ''
                )

            else:

                answer = input()

        #remove EOF
        answer = answer.replace("EOF", "")

    else:

        #set defualt kwargs['message'] if empty
        if not kwargs['message']:

            kwargs['message'] = format(

                "Please enter the input (terminate input by pressing enter"
            )

        #check if input should be secure
        if kwargs.get("secure", False):

            answer = getpass(

                prompt = kwargs['message']
            )

        else:

            answer = input(

                kwargs['message']
            )

        #while input is empty
        while not answer:

            #check if input should be secure
            if kwargs.get("secure", False):

                answer = getpass(

                    prompt = kwargs['message']
                )

            else:

                answer = input(

                    kwargs['message']
                )

    #check if answer should be converted
    if kwargs.get("convert", False):

        if kwargs['convert'].lower() == "int":

            try:

                tmp = answer
                answer = int(

                    answer
                )

            except:

                pass
                answer = tmp

        elif kwargs['convert'].lower() == "float":

            try:

                tmp = answer
                answer = float(

                    answer
                )

            except:

                pass
                answer = tmp

    return answer

def menu(templates, **kwargs):

    data_type = set([type(i).__name__ for i in templates.keys()])

    found, answer = "", ""


    #if keys are str
    if 'str' in data_type:

        #sort keys by number in a dict under template_key
        navigation = {

            key: value[0] for key, value in \
            enumerate(

                sorted(

                    templates.items(), key = \
                    lambda x: x[1][OPTIONS['get_templates']['template_number']]
                ), 1

            ) if not templates[value[0]].get("hidden", False)
        }

    #keys expected to be number (float)
    else:

        #sort by template key
        navigation = {

            key: value for key, value in \
            enumerate(

                sorted(

                    templates.keys()
                    ), 1

            ) if not templates[key].get("hidden", False)
        }

    #if menu number provided
    if kwargs.get("menu_number", False):

        try:

            answer = int(kwargs['menu_number'])

        except:

            pass
            answer = ""

    else:

        ### Display menu ####

        print("Menu: \n")

        for i in sorted(navigation.keys()):

            print(i, ".", templates[navigation[i]]['name'])

        #message for get user input
        message = format(

            "\n\nPlease choose your option: (End your input by pressing enter)\n"
        )

        ### getting user input ####

        #get initial answer
        answer = get_user_input(

            message = message,
            convert = "int"
        )

    #check the answer
    if kwargs.get("menu_number", False):

        #if menu num not found or empty
        if not answer or answer not in navigation:

            return ""
    else:

        #while answer is incorect then keep asking
        while not answer in navigation:

            answer = get_user_input(

                message = message,
                convert = "int"
            )

    ### choosing correct dodo ###

    if templates[navigation[answer]].get(
    OPTIONS['get_templates']['shadows_name'], False):

        for shadow_key, shadow_value in templates[navigation[answer]]\
        [OPTIONS['get_templates']['shadows_name']].items():

            for internal_key, interval_value in shadow_value.items():

                #if key == name then jump to next key
                if internal_key == OPTIONS['get_templates']['shadow_name']:

                    #clear found
                    found = ""

                    #jump to next
                    continue

                #key_name is missing in kwargs then break this loop
                if not kwargs.get(internal_key, ""):

                    #clear found
                    found = ""

                    #exit loop
                    break

                #break loop because value from template not eqaul to kwargs
                if not interval_value.lower() in kwargs[internal_key].lower():

                    #clear found
                    found = ""

                    #exit loop
                    break

                found = shadow_key

            #if key is found then break 1st loop
            if found:

                break

        #find correct key
        for key, value in templates.items():


            if (found == value[OPTIONS['get_templates']['template_number']]
            or found == value[OPTIONS['get_templates']['template_name']]):

                found = key

                #exit loop as correct key found
                break
    else:

        found = navigation[answer]

    #if key is not fund then assign master todo key
    if not found:

        found = navigation[answer]

    return found

def run_cmd(ip, cmd, **kwargs):

    result={
        'ip' : ip,
        'cmd': cmd,
        'output' : '',
        'status' : ''
    }

    #command
    command = ""

    ''' setting command base on kwargs '''

    if kwargs.get("local", False):

            command=format(cmd)
    else:

        if kwargs.get("command_switch", False):

            cmd=format(
            kwargs['command_switch']+" "+cmd
            )

        if kwargs.get("tool", False):

            command=format(
            kwargs['tool']+" "+ip +" "+cmd+" "
            )

            if kwargs.get("reverse", False):

                command=format(
                kwargs['tool']+" "+cmd+" "+ip
                )
        else:

            command=format(
            cmd+" "+ip
            )
    try:

        ''' running cmd '''

        result['output']=subprocess.check_output(command,
        universal_newlines=True,
        shell=True,
        stderr=subprocess.STDOUT
        )

        #setting status to fine
        result['status']='ok'

    except subprocess.CalledProcessError as exc:

        pass

        print("Running cmd: ",exc.cmd,
        "has finished with exit status: ",exc.returncode,
        "\nError: ",exc.output)

        #setting status to failed
        result['status']='failed'

        #clearing result
        result['output']=''

    return result

if __name__ == '__main__':

    #loading templates to dictionary
    CMD = get_templates(

        ARGS.template_dir, "number"
    )

    key = menu(

        CMD,
        dir = "/tmp/templates",
        powietrze = "Tak"
    )

    cmd_opt = CMD[key]['commands'] if CMD[key].get('commands', False) else {}

    result = run_cmd("", CMD[key]['text'], **cmd_opt)

    print("Cmd: ", result['cmd'],
    "\nStatus: ", result['status'],
    "\nResult:\n", result['output'])
