#!/usr/bin/python

import webbrowser
import argparse
from os.path import exists


def isPath(path):

    if exists(path):

        return path
    else:

        message=format(
        "\nPath: "+path
        +" doesn't exist !!!!"
        )

        raise argparse.ArgumentTypeError(message)

if __name__ == '__main__':

    ''' argparse '''

    parser = argparse.ArgumentParser()
    parser.add_argument('urls', nargs='*', help='url1 url2 urlN')
    parser.add_argument('-file', '--filename', dest='file',
        help='Full path to file with urls, separator is new line/',
        type=isPath)

    ARGS=parser.parse_args()

    if not ARGS.file:

        for url in ARGS.urls:

            webbrowser.open_new_tab(url)

    else:

        with open(ARGS.file, 'r', encoding='utf_8') as urls:

            for url in urls.read().split('\n'):

                if url.strip():

                    webbrowser.open_new_tab(url.strip())
