#!/usr/bin/python3

import tokenize

''' generator of fib '''

def fibonnaci():

    a, b = 0, 1

    while True:

        yield b

        a, b = b, a+b

def power(values):

    for value in values:

        print('zasilam %s' % value)

        yield value

def adder(values):

    for value in values:

        print('dodaje do %s' %value)

        if value %2 == 0:

            yield value+3

        else:

            yield value+2

if __name__ == '__main__':

    auto_path='/home/smieszek/Dokumenty/skrypty/python/pyautogui/auto.py'

    #generator
    fib=fibonnaci()

    #iterating over generator
    print([next(fib) for i in range(13)])

    ''' tokenize '''

    tokens = tokenize.generate_tokens(
    open(auto_path).readline
    )

    for cnt, i in enumerate(range(10),1):

        print("Line: ",cnt," ",next(tokens))

    ''' smaller generators '''

    elements = [1, 4, 7, 9, 12, 19]

    results=adder(power(elements))

    for i in range(5):

        print(next(results))
    
