#!/usr/bin/python3
from time import sleep
import multiprocessing
import timeit


opt={
'myvar' :2
}

def fun1(sec, result):

    print("I'll wait for: ",sec)
    print(opt['myvar'])
    opt['myvar']=5
    sleep(sec)
    result['fun1']="It is function 1"

def fun2(sec, result):

    print("I'll wait for: ",sec)
    sleep(sec)
    result['fun2']="It is function 2"

def fun3(sec, result):

    print("I'll wait for: ",sec)
    sleep(sec)
    result['fun3']="It is function 3"

def fun4(sec, result):

    print("I'll wait for: ",sec)
    sleep(sec)
    result['fun4']="It is function 4"

if __name__ == '__main__':

    multiprocessing.set_start_method('spawn')

    result=multiprocessing.Manager().dict()

    config={

        'fun1' :
        {
            'name': fun1,
            'arg' : (5, result),
            'process' : None,
        },

        'fun2' :
        {
            'name': fun2,
            'arg' : (5, result),
            'process' : None,
        },

        'fun3' :
        {
            'name': fun3,
            'arg' : (5, result),
            'process' : None,
        },

        'fun4' :
        {
            'name': fun4,
            'arg' : (5, result),
            'process' : None,
        },
    }


    for key in config.keys():

        config[key]['process']=multiprocessing.Process(target=config[key]['name'],
        args=config[key]['arg'])
        config[key]['process'].start()

    for key in config.keys():

        config[key]['process'].join()

    summary=format(
    "Function 1" + result['fun1']
    +"\nFunction 2:" +result['fun2']
    +"\nFunction 3: "+result['fun3']
    +"\nFunction 4:" +result['fun4']
    )
    print(summary,opt)
