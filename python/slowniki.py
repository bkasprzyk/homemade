#!/usr/bin/python3

from collections import OrderedDict
from types import MappingProxyType

src=[
('one',1),
('two',2),
('three',3)]

print({key:val for key,val in src})

d=OrderedDict({
    'a':
    {
        'b':{
            'a':1
            }
    }
})

print(d)
d.clear()
print(d,type(d))

d={

    'a' : 1,
    'b' : False
}

print(">",d.get('b',False))

text=format(
"First Line: "
+"\nSecond Line: "
+"\nThird Line"
)

for line, cont in enumerate(text.split("\n"),1):
    print("Line: ",line,"\tCont: ",cont)
dp=MappingProxyType(d)
print(dp,type(dp))
d['a']=3
print(dp,type(dp))

print(">",set({'a':1,'b':2,'a':3}),"\n\n>>",set([1,2,3,4,5,6,7]))
needles=[1,2]
haystack=[10,9,9,8,888,7,6,7,5,4,3,2,1]
print(set(needles) & set(haystack))
