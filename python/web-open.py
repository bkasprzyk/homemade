#!/usr/bin/python

import pyperclip, webbrowser,urllib2, re, sys
from pynput.keyboard import Key,Controller
from sys import exit
from time import sleep
from platform import system


if __name__ == '__main__':

    #controller object
    keyboard = Controller()

    #delay
    keyboard_dealy=0.5

    #url
    url=format(
    "https://www.up.krakow.pl/"
    )

    ''' Press and realease  keys '''

    if system() != "Darwin":

        #simulate ctrl +c
        keyboard.press(Key.ctrl)
        keyboard.press('c')

        #sleep
        sleep(keyboard_dealy)

        #release keys
        keyboard.release('c')
        keyboard.release(Key.ctrl)

    else:

        #simulate cmd +c
        keyboard.press(Key.cmd)
        keyboard.press('c')

        #sleep
        sleep(keyboard_dealy)

        #release keys
        keyboard.release('c')
        keyboard.release(Key.cmd)

    ''' working with url '''

    #concat url + copied content
    url+=pyperclip.paste()

    #open the url with copied content
    webbrowser.open_new_tab(url)
