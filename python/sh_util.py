#!/usr/bin/python3

import zipfile
import os
import prettytable
import sys


def getDirContent(path):

    #column names
    table_header=['Path', 'Directories', 'Files']

    #create object of Pretty table with all borders
    result=prettytable.PrettyTable(hrules=prettytable.ALL, header=True,
    padding_width=3)

    #add column names
    result.field_names=table_header

    for fpath, directories, filenames in os.walk(path):

        ''' creating a row '''

        #adding path
        row=[fpath]

        #adding directories
        row.append("-") if not directories else \
        row.append('\n'.join(directories))

        #adding files
        row.append("-") if not filenames else \
        row.append('\n'.join(filenames))

        ''' adding row '''

        result.add_row(row)

        #clearing
        row.clear()

    ''' returning formated result '''

    return format(result)

def unpackZip(src, dst):

    isOK=False

    try:

        zip=zipfile.ZipFile(src)
        zip.extractall(path=dst)
        zip.close()
        isOK=True

    except:

        pass
        isOK=False
        print("Soemthing went wrong.",
        "\nFile: ",src," could not be opened!\n",
        sys.exc_info())

    return isOK

def createZip(dst,filename, *args):

    isOK=False

    #path to file
    path=(dst+filename) if dst.endswith('/') else (dst+"/"+filename)

    try:

        zip=zipfile.ZipFile(path,'w')

        for file in args:
            zip.write(file, compress_type=zipfile.ZIP_DEFLATED)

        zip.close()

        isOK=True

    except:

        pass
        isOK=False
        print("Soemthing went wrong.",
        "\nFile: ",path," could not be created!\n",
        sys.exc_info())

    return isOK

if __name__ == '__main__':

    print(
    "Before file is created:\n",
    getDirContent('/tmp')
    )

    createZip("/tmp/dest","moje.zip",'/tmp/test.txt')

    print(
    "After file is created:\n",
    getDirContent('/tmp')
    )
