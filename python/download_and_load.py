#!/usr/bin/python2

import sys
import os
import subprocess
import urllib2
import getpass
from time import time, sleep

OPTIONS = {

    'cmd' :
    {

        'get_disks' : (

            "ls /dev/nvm*n1 2>/dev/null " +
            "|| ls /dev/sd* 2>/dev/null" +
            " |grep -E -v \'[a-z][0-9]\'" +
            "|| ls /dev/hd* 2>/dev/null" +
            " |grep -E -v \'[a-z][0-9]\'"
        ),

        'get_cdroms' : (

            "ls /dev/sr* 2>/dev/null"
        ),

        'get_cdrom_data' : (

            "cat /sys/block/"

        )
    },

    'status' :
    {
        'ok_status' : 'ok',
        'failed_status' : 'failed'
    },

    'cdrom' :
    {

        'model' : '',
        'vendor' : '',
        'model_text' : 'device/model',
        'vendor_text' : 'device/vendor'

    },

    'download_file' :
    {
        'url' : ('https://cdimage.debian.org/debian-cd/current' +
        '/amd64/iso-cd/debian-10.10.0-amd64-netinst.iso'),

        'path' : '~/plik.iso'
    },

    'misc' :
    {
        'required_users' : ['root'],
        'mnt_dir' : '~/mnt_dir',
        'partition_number' : 1
    }
}

def run_cmd(cmd):

    """
    run_cmd(cmd)

    where

        cmd is command to run

    Function is used to run commands
    Functions returns:

    result = {

        'cmd': cmd,
        'output' : '',
        'status' : '' (ok | failed)
    }
    """

    result = {

        'cmd': cmd,
        'output' : '',
        'status' : ''
    }

    try:

        ### running cmd ###

        result['output'] = subprocess.Popen(

            cmd,
            shell = True,
            stdout = subprocess.PIPE,
            universal_newlines = True
        )

        #wait
        result['output'].wait()

        #output
        result['output'] = result['output'].communicate()[0]

        #setting status to fine
        result['status'] = OPTIONS['status']['ok_status']

    except subprocess.CalledProcessError:

        pass

        print("Running cmd: ", cmd, " has failed:\n", sys.exc_info())

        #setting status to failed
        result['status'] = OPTIONS['status']['failed_status']

        #clearing result
        result['output'] = ''

    return result

def get_disks():

    """
    get_disks()

    Function is used to get list of disk (without partitions)
    Functions returns:

    result = {

        'disks' : [],
        'disks_raw' : []
        'status' : '' (ok | failed)
    }
    """

    result = {

        'status' : OPTIONS['status']['failed_status'],
        'disks' : [],
        'disks_raw' : []
    }

    ### run cmd ###

    disks = run_cmd(

        OPTIONS['cmd']['get_disks']

    )

    ### check status of cmd ###

    if disks['status'] == OPTIONS['status']['ok_status']:

        for element in disks['output'].split("\n"):

            if element.strip():

                #full path
                result['disks'].append(

                    element.strip()
                )

                #only raw device name
                result['disks_raw'].append(

                    element.strip().split("/")[2]
                )

        #set status as good
        result['status'] = OPTIONS['status']['ok_status']

    return result

def get_cdroms():

    """
    get_cdroms()

    Function is used to get list of croms matching criteria
    Functions returns:

    result = {

        'cdroms' : [],
        'cdroms_raw' : []
        'status' : '' (ok | failed)
    }
    """

    result = {

        'status' : OPTIONS['status']['failed_status'],
        'cdroms' : [],
        'cdroms_raw' : []
    }

    ### run cmd ###

    cdroms = run_cmd(

        OPTIONS['cmd']['get_cdroms']

    )

    ### check status of cmd ###

    if cdroms['status'] == OPTIONS['status']['ok_status']:

        for element in cdroms['output'].split("\n"):

            if element.strip():

                #full path
                device_name = element.strip()

                #raw device name
                device_name_raw = device_name.split("/")[2]

                #get cdrom model
                model = run_cmd(

                    OPTIONS['cmd']['get_cdrom_data'] + device_name_raw +
                    "/" + OPTIONS['cdrom']['model_text']

                )

                #get cdrom model
                vendor = run_cmd(

                    OPTIONS['cmd']['get_cdrom_data'] + device_name_raw +
                    "/" + OPTIONS['cdrom']['vendor_text']

                )

                ### check status of cmd ###

                if (model['status'] == OPTIONS['status']['ok_status']
                and vendor['status'] == OPTIONS['status']['ok_status']):

                    ### check if properties matches ###

                    if (OPTIONS['cdrom']['model'].lower()
                    in model['output'].lower() and
                    OPTIONS['cdrom']['vendor'].lower()
                    in vendor['output'].lower()):

                        #full path
                        result['cdroms'].append(

                            device_name
                        )

                        #only raw device name
                        result['cdroms_raw'].append(

                            device_name_raw
                        )

        #set status as good
        result['status'] = OPTIONS['status']['ok_status']

    return result

def download_file(url, dst):

    """
    download_file(url)

    where

        url is url link
        dst full path to the destination file

    Function is used to donload file from specified url
    Functions returns:

    result = {

        'url': url,
        'filename' : dst,
        'status' : '' (ok | failed)
    }
    """

    result = {

        'url': url,
        'filename' : dst,
        'status' : OPTIONS['status']['failed_status']
    }

    #start time
    st = time()

    #inform user
    print("Download file from: ", url)

    ### download and save the file ###

    try:

        #download dile
        download = urllib2.urlopen(

            url
        )

        #open dst file-cd/curre
        file = open(

            dst,
            'wb'
        )

        #write to dst file
        file.write(

            download.read()
        )

        #mark as good
        result['status'] = OPTIONS['status']['ok_status']

        #close dst file
        file.close()

        #close download
        download.close()

    except KeyboardInterrupt:

        raise

    except:

        pass
        print("Failed to download a file from:\n", url,
        "\n", sys.exc_info())

        #mark as bad
        result['status'] = OPTIONS['status']['failed_status']

    #inform user
    print("Downloading file from: ", url,"\nTook: ",
    round(time() - st, 2), " (s)")

    return result


if __name__ == "__main__":

    ### check user priveleges ###

    #get user name
    user = getpass.getuser()

    if user.lower() not in OPTIONS['misc']['required_users']:

        print("Root priveleges are required to run this script\n",
        "\nexiting!")

        sys.exit(1)

    ### create mnt dir ###

    #make directory
    mkdir = run_cmd(

        "mkdir -p " + OPTIONS['misc']['mnt_dir']
    )

    #check if directory was made as expected
    if mkdir['status'] == OPTIONS['status']['failed_status']:

        print("Could not make directory:",
        OPTIONS['misc']['mnt_dir'], "\nexiting!")

        sys.exit(1)

    ### Downloading file ###

    #download file
    downloading = download_file(

        OPTIONS['download_file']['url'],
        OPTIONS['download_file']['path']
    )

    #check status of downloading file
    if downloading['status'] == OPTIONS['status']['failed_status']:

        print("Download file from below url, went wrong:\n",
        OPTIONS['download_file']['url'], "\nexiting!")

        sys.exit(1)

    ### getting cdroms ###

    #get available cdroms
    cdroms = get_cdroms()

    #check available cdroms
    if (cdroms['status'] == OPTIONS['status']['failed_status']
    or not cdroms['cdroms']):

        print("No cdroms were found or smth failed", "\nexiting!")

        sys.exit(1)

    for cdrom in cdroms['cdroms']:

        #get current disks
        current_disks = get_disks()

        #check current disks
        if (current_disks['status'] == OPTIONS['status']['failed_status']
        or not current_disks['disks']):

            print("No disks were found or smth failed",
            "\nJump to next!")

            #jump next
            continue

        ### ejecting cdrom ###

        eject_cdrom = run_cmd(

            "eject " + cdrom
        )

        #check ejecting status
        if (eject_cdrom['status'] == OPTIONS['status']['failed_status']):

            print("Could not eject cdrom: ", cdrom,
            "\nJump to next!")

            #jump to next
            continue

        ### find new disks ###

        #get new disk
        new_disks = get_disks()

        #check current disks
        if (new_disks['status'] == OPTIONS['status']['failed_status']
        or not new_disks['disks']):

            print("No disks were found or smth failed",
            "\nJump to next!")

            #jump next
            continue

        #if more disks are found
        if (len( set(new_disks['disks']) - set(current_disks['disks']) ) != 1:

            print("Found more  or none new disks",
            "\nJump to next!")

            #jump next
            continue

        #new disk
        new_disk = set(new_disks['disks']) - set(current_disks['disks'])

        #get first el of a new disk
        for i in new_disk:

            new_disk = i

            break

        ### mount disk and perform file operations ###

        #mount partition
        mount_partition = run_cmd(

            ("mount " + new_disk + OPTIONS['partition_number'] + " " +
            OPTIONS['misc']['mnt_dir'])
        )

        #check status of mounting partition
        if mount_partition['status'] == OPTIONS['status']['failed_status']:

            print("Could not mount the partition",
            "\nJump to next!")

            #jump next
            continue

        ### copy file ###

        #copy file and sync
        copy_file = run_cmd(

            ("cp " + OPTIONS['download_file']['path'] + " " +
            OPTIONS['misc']['mnt_dir'] + " ; sync")
        )

        #check status of copy_file
        if copy_file['status'] == OPTIONS['status']['failed_status']:

            print("Could not copy the file: ",
            OPTIONS['download_file']['path'], "\nJump to next!")

            #jump next
            continue

        ### umount partition and eject ###

        #umounr partition
        umount_partition = run_cmd(

            ("umount "  + OPTIONS['misc']['mnt_dir'])
        )

        #check status of umount partition
        if copy_file['status'] == OPTIONS['status']['failed_status']:

            print("Could not umount: ",
            OPTIONS['misc']['mnt_dir'], "\nJump to next!")

            #jump next
            continue

        ### ejecting cdrom ###

        eject_disk = run_cmd(

            "eject " + new_disk
        )

        #check ejecting status
        if (eject_disk['status'] == OPTIONS['status']['failed_status']):

            print("Could not eject disk: ", new_disk,
            "\nJump to next!")

            #jump to next
            continue

        print("Cdrom: ", cdrom, " was updated with latest version")
