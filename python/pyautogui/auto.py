#!/usr/bin/python3

import sys
import pyautogui as pt
from time import sleep

COORDINATES={
    'menu':
    {
        'x' : 16,
        'y': 1060
    },

    'draw-rectangle-position':
    {
        'x' : 26,
        'y' : 273,
    },

    'draw-move-with-rectangle':
    {
        'x' : 764 ,
        'y' : 340
    },

    'draw-rectangle-drag-rel':
    {
        'x' : 100,
        'y' : 150
    },

    'draw-rectangle-drew-position':
    {
        'x' : 818,
        'y' : 426
    },

    'draw-rectangle-drag-copy-rel':
    {
        'x' : 200,
        'y' : None
    },

    'draw-elipse-position':
    {
        'x' : 17,
        'y' : 307
    },

    'draw-with-elipse-position':
    {
        'x' : 760,
        'y' : 628
    },

    'draw-elipse-drag':
    {
        'x' : 1069,
        'y' : 760,

    },

    'draw-elipse-move-away':
    {
        'x' : 1080,
        'y' : 776
    }
}

def openWhiskerMenu():

    #click on whisker menu
    pt.click(COORDINATES['menu']['x'], COORDINATES['menu']['y'],
    button='left')

    #sleep 1 sec
    sleep(1)

def openLibreOfficeDraw():

    #type rys
    pt.typewrite('rys', interval=0.25)

    #press enter
    pt.hotkey('enter')

    #sleep 10 sec
    sleep(10)

def getLibreOfficeRectangle():

    #move to rectangle (toolbar)
    pt.moveTo(COORDINATES['draw-rectangle-position']['x'],
    COORDINATES['draw-rectangle-position']['y'])

    #click on rectangle (toolbar)
    pt.click(COORDINATES['draw-rectangle-position']['x'],
    COORDINATES['draw-rectangle-position']['y'])

def makeLibreOfficeRectangle():

    #move cursor with  rectangle to position
    pt.moveTo(COORDINATES['draw-move-with-rectangle']['x'],
    COORDINATES['draw-move-with-rectangle']['y'])

    #drag rectangle to relative position
    pt.dragRel(COORDINATES['draw-rectangle-drag-rel']['x'],
    COORDINATES['draw-rectangle-drag-rel']['y'])

def copyLibreOfficeRectangle():

    #move cursor to position of existing rectangle
    pt.moveTo(COORDINATES['draw-rectangle-drew-position']['x'],
    COORDINATES['draw-rectangle-drew-position']['y'])

    #click on rectangle
    pt.click(COORDINATES['draw-rectangle-drew-position']['x'],
    COORDINATES['draw-rectangle-drew-position']['y'])

    #copy rectangle
    pt.hotkey('ctrl','c')

    #paste rectangle
    pt.hotkey('ctrl','v')

    #sleep 2 sec
    sleep(2)

def moveCopiedLibreOfficeRectangle():

    #move copied rectangle to relative position
    pt.dragRel(COORDINATES['draw-rectangle-drag-copy-rel']['x'],
    COORDINATES['draw-rectangle-drag-copy-rel']['y'])

def getLibreOfficeElipse():

    #move to elipse (toolbar)
    pt.moveTo(COORDINATES['draw-elipse-position']['x'],
    COORDINATES['draw-elipse-position']['y'])

    #click on elipse (toolbar)
    pt.click(COORDINATES['draw-elipse-position']['x'],
    COORDINATES['draw-elipse-position']['y'])

def makeLibreOfficeElipse():

    #move cursor with  elipse to position
    pt.moveTo(COORDINATES['draw-with-elipse-position']['x'],
    COORDINATES['draw-with-elipse-position']['y'])

    #drag elipse to  position
    pt.dragTo(COORDINATES['draw-elipse-drag']['x'],
    COORDINATES['draw-elipse-drag']['y'])

    #move away
    pt.moveTo(COORDINATES['draw-elipse-move-away']['x'],
    COORDINATES['draw-elipse-move-away']['y'])

    #click after move
    pt.click(COORDINATES['draw-elipse-move-away']['x'],
    COORDINATES['draw-elipse-move-away']['y'])

if __name__ == '__main__':

    ''' Open whisker Menu '''

    openWhiskerMenu()

    ''' Open LibreOffice Draw '''

    openLibreOfficeDraw()

    ''' select rectangle '''

    getLibreOfficeRectangle()

    ''' make rectangle '''

    makeLibreOfficeRectangle()

    ''' copy rectangle '''

    copyLibreOfficeRectangle()

    ''' move copied rectangle '''

    moveCopiedLibreOfficeRectangle()

    ''' get elipse '''

    getLibreOfficeElipse()

    ''' make an elipse '''

    makeLibreOfficeElipse()
