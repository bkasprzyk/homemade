#!/usr/bin/python3

import sys
import pyautogui as pt
from time import sleep

if __name__ == '__main__':

    ''' make screen shoot  and saving'''

    im=pt.screenshot()

    #saving screenshot
    im.save('/tmp/plik.jpg')

    ''' get coordinates based on cursor movement '''

    #pt.displayMousePosition()

    ''' messages '''

    #alert confirm only
    pt.alert("I am showing an alert :)")

    #confirm or cancel
    pt.confirm("Please confirm :)")

    #ask for input
    answer=pt.prompt('Do you like it?')
    print("Here is my answer: ",answer)

    #getting password
    answer=pt.password("Gimme your passwd",title="Password prompt")
    print("Your password is: ",answer)
