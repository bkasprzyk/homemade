#!/usr/bin/python3

import xlsxwriter
import sys
from collections import OrderedDict

def data2Excel(source, destination, fieldnames):

    #create workbook
    wb = xlsxwriter.Workbook(destination)

    ### default format ###

    #setting border
    cell_format = wb.add_format(

        {
            'border': 2
        }
    )

    #wrap text in a cell
    cell_format.set_text_wrap()

    #align vertical center
    cell_format.set_align(

        'top'
    )

    ### header  celll format ###

    #setting border
    header_format = wb.add_format(

        {
            'border': 2
        }
    )

    #align vertical center
    header_format.set_align(

        'top'
    )

    #font size
    header_format.set_font_size(

        14
    )

    #bg color
    header_format.set_bg_color(

        '#D3D3D3'
    )

    ### Work Sheets ###

    #ws dictionary with work sheets
    ws = {}

    for data in source.values():

        if not data["sheet_name"] in ws.keys():

            #creating work sheet if not exist
            ws[data['sheet_name']] = {

                'worksheet' : wb.add_worksheet(

                    data['sheet_name']
                ),

                'row' : 0
            }

            ### adding header ###

            for column, fieldname in enumerate(fieldnames.keys(), 0):

                #writing to columns at row 0
                ws[data['sheet_name']]['worksheet'].write(

                    ws[data['sheet_name']]['row'],
                    column,
                    fieldnames[fieldname],
                    header_format
                )

            #increase row number() once header is added
            ws[data['sheet_name']]['row'] += 1

        #feeding work sheet (adding data)
        for column, fieldname in  enumerate(fieldnames.keys(), 0):

            ''' writing to columns at row N '''

            ws[data['sheet_name']]['worksheet'].write(

                ws[data['sheet_name']]['row'],
                column,
                data[fieldname],
                cell_format
            )

        #increase row number
        ws[data['sheet_name']]['row'] += 1

    ### Enable autofilter in worksheet ###

    for i in ws.keys():


        #autofilter
        ws[i]['worksheet'].autofilter(

            0,
            0,
            ws[i]['row'],
            len(fieldnames.keys()) - 1
        )

        ### adjust size of columns ###

        for column, fieldname in  enumerate(fieldnames.keys(), 0):

            #setting width size of a column as size of header column +10
            ws[i]['worksheet'].set_column(

                column,
                column,
                len(str(fieldnames[fieldname])) + 10
            )

    #close workbook
    wb.close()


if __name__ == '__main__':

    filename = '/tmp/data.xlsx'

    fieldnames = OrderedDict({

        'name' : 'Name',
        'surname' : 'Surname',
        'shoe_size' : 'Shoe Size',
        'phone_extension' : 'Phone Extension',
        'phone_number' : 'Phone Number'
    })

    dataset = {

        'data1':
        {
            'sheet_name' : 'Secure',
            'name' : 'Adam',
            'surname' : 'SomeSurname2hhbhebehbfhbfhbfhebhebfhebfhbfhebfhebhebhe',
            'shoe_size' : 39,
            'phone_extension' : '+41' ,
            'phone_number' : '143_222_333'
        },

        'data2':
        {
            'sheet_name' : 'Insecure',
            'name' : 'Alan',
            'surname' : 'SomeSurnamenjnjnjnjnjnjnjnjnnjnjjjnjnjnjn',
            'shoe_size' : 38,
            'phone_extension' : '+48' ,
            'phone_number' : '121_222_333'
        },

        'data3':
        {
            'sheet_name' : 'Secure',
            'name' : 'Tom',
            'surname' : 'SomeSurname2',
            'shoe_size' : 40,
            'phone_extension' : '+58' ,
            'phone_number' : '131_222_333'
        },

        'data4':
        {
            'sheet_name' : 'Insecure',
            'name' : 'Cate',
            'surname' : 'SomeSurname3',
            'shoe_size' : 41,
            'phone_extension' : '+68' ,
            'phone_number' : '151_222_333'
        }
    }

    data2Excel(dataset, filename, fieldnames)
