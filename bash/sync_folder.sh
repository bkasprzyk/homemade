#!/bin/bash

#SCRIPT IS USED TO SYCNRHONIZE REMOTE FOLDER WITH local FOLDER
#using rclone

init()
{

  #varables

  LOCAL_DIRECTORY="$1"
  REMOTE_RESOURCE="$2"
  #1 way remote to local
  TWO_WAY_SYNC=${3:-"no"}
  MOUNT_DIRECTORY=$(mktemp -d)
  PING_ADDRESS='8.8.8.8'
  TRIES="2"

}

test_connection()
{

  #Functions test if network connection is available

  #varabiables
  local status
  status=""

  #test connection
  ping "$1" -c "$2" >/dev/null 2>&1 ; status=$?

  #returning result
  echo "$status"

}

drive_operation()
{

  #mount / umount operation
  if [ "$1" == "mount" ]
  then

    rclone mount --daemon "$REMOTE_RESOURCE" "$MOUNT_DIRECTORY"
    sleep 5

  elif [ "$1" == "umount" ]
  then

    sleep 5
    fusermount -uz "$MOUNT_DIRECTORY"

  fi

}

sync_folder()
{

  for i in $(ls "$MOUNT_DIRECTORY/")
  do

    for j in $(ls "$LOCAL_DIRECTORY")
    do

      #if file names are the same
      if [ "$i" == "$j" ]
      then

        #if only remote to local
        if [ "$TWO_WAY_SYNC" == "no" ] || [ "$TWO_WAY_SYNC" == "NO" ]
        then

          #if remote is newer than local then copy remote to local
          if [ "$MOUNT_DIRECTORY/$i" -nt "$LOCAL_DIRECTORY/$j" ]
          then

            cp "$MOUNT_DIRECTORY/$i"  "$LOCAL_DIRECTORY/$j"

          fi

        else

          #if remote is newer than local then copy remote to local
          if [ "$MOUNT_DIRECTORY/$i" -nt "$LOCAL_DIRECTORY/$j" ]
          then

            cp "$MOUNT_DIRECTORY/$i"  "$LOCAL_DIRECTORY/$j"

          else

            cp "$LOCAL_DIRECTORY/$j" "$MOUNT_DIRECTORY/$i"

          fi

        fi

      fi

    done

  done

}

main ()
{

  #init variables
  init "$@"

  #check connection
  is_connection=$(test_connection "$PING_ADDRESS" "$TRIES")

  if [ "$is_connection" -eq 0 ]
  then

      #mount remote
      drive_operation "mount"

      #sync folder
      sync_folder

      #umount remote
      drive_operation "umount"

  fi

}

##### Main ###

main "$@"
