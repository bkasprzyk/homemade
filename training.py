#!/usr/bin/python3

import heapq
import random
import textwrap
import csv
import sys
import prettytable
from collections import deque, Counter, ChainMap
from operator import itemgetter
from itertools import groupby, chain
from datetime import datetime, timedelta
from time import time, sleep
from io import StringIO, BytesIO
from json import loads, dumps


### functions ###
def int_only(a :int, b:int):

    print(a+b)

def search(lines, pattern, history=5):

    previous_lines = deque(maxlen=history)
    for line in lines:

        if pattern in line:

            yield line, previous_lines

        previous_lines.append(line)

class PriorityQueue:

    def __init__(self):

        self._queue = []
        self._index = 0

    def push(self, item, priority):

        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]

class Item:

    def __init__(self, name):

        self.name = name

    def __repr__(self):

        return 'Item({!r})'.format(self.name)

def get_previous_byday(dayname, start_date=None):

    if start_date is None:

        start_date = datetime.today()

    day_num = start_date.weekday()
    day_num_target = weekdays.index(dayname)
    days_ago = (7 + day_num - day_num_target) % 7

    if days_ago == 0:

        days_ago = 7

    target_date = start_date - timedelta(days=days_ago)

    return target_date

def csv_write(src, dst, **kwargs):

    #store the status
    result = {

        'isOK' : False,
        'fname' : dst
    }

    if type(src).__name__ != "list":

        return result

    try:

        with open(dst, "w+") as csv_file:

            csvwriter = csv.writer(csv_file)

            for record in src:

                csvwriter.writerow(record)

            csv_file.close()

        #mark as good
        result['isOK'] = True

    except:

        print("Could not create a file: ", dst,
        "\n", sys.exc_info())
        pass

    return result

def csv_read(src, **kwargs):

    #store the status
    result = {

        'isOK' : False,
        'records' : []
    }

    if type(src).__name__ != "str":

        return result

    try:

        with open(src, "r") as csv_file:

            csvreader = csv.reader(csv_file)

            for record in csvreader:

                result['records'].append(

                    record
                )

            csv_file.close()

        #mark as good
        result['isOK'] = True

    except:

        print("Could not open a file: ", dst,
        "\n", sys.exc_info())
        pass

    return result

class Cache:

    def __init__(self, ttl = 500 ):

        self._data = {}
        self._data_ttl = {}
        self._ttl = ttl;

    def __getitem__(self, key):

        if time() - self._data_ttl[key] > self._ttl:

            del self._data[key]

        return self._data.get(key)

    def __setitem__(self, key, item):

        self._data[key] = item
        self._data_ttl[key] = time()

if __name__ == '__main__':


    ### unpack list to variables ###

    a, b, c = ['a', 'b', 'c']

    print(a, b, c)

    ### unpack many using * ###

    a = [

        [1, 2, 3],
        [4, 5, 6, 7]
    ]

    for record in a:

        a, b, *c = record

        print("a: ", a,
        "\nb: ", b,
        "\nc: ", c)

        print("Next iteration")

    ### unpack many and ignore using * ###

    a = [0, 1, 2, 3]

    *_, b = a

    print("whole list: ", a, "\nb: ",b)

    ### head and tail using * ###

    a = ['a', 'b', 'c', 'd']

    head, *tail = a

    print("Whole list: ", a,
    "\nHead: ", head,
    "\nTail: ", tail)

    ### first and last el of list ###


    a = ['a', 'b', 'c', 'd']

    first, *_ = a
    *_, last = a

    print("Whole list: ", a,
    "\nFirst: ", first,
    "\nLast: ", last)


    ### top largest and top smallest

    size = 20

    numbers = [random.randrange(-100, 100) for i in range(1, size)]

    print("Random list:\n", numbers,
    "\nTop 5 largest", heapq.nlargest(5, numbers),
    "\nTop 5 smallest", heapq.nsmallest(5, numbers))

    ### top largest and top smallest (complex ds) ###

    students = [

        {'Name' : 'Anthony', 'Surname' : 'ABC', 'avg' : 3.5},
        {'Name' : 'Anthony', 'Surname' : 'ABD', 'avg' : 4.55},
        {'Name' : 'Anthony', 'Surname' : 'ABE', 'avg' : 4.6},
        {'Name' : 'Anthony', 'Surname' : 'ABA', 'avg' : 5.0},
        {'Name' : 'Anthony', 'Surname' : 'ABS', 'avg' : 4.7},
        {'Name' : 'Anthony', 'Surname' : 'ABY', 'avg' : 2.5},
        {'Name' : 'Anthony', 'Surname' : 'ABT', 'avg' : 4.8},
        {'Name' : 'Anthony', 'Surname' : 'ABM', 'avg' : 3.0},
        {'Name' : 'Anthony', 'Surname' : 'ABH', 'avg' : 3.7},
        {'Name' : 'Anthony', 'Surname' : 'ABF', 'avg' : 3.2}
    ]

    print(
        '\nName Surname Avg\n\n',
        '\n'.join(i['Name'] + ' ' + i['Surname'] + ' ' + str(i['avg']) \
        for i in students), sep =''
    )

    high = heapq.nlargest(5, students, key = lambda s: s['avg'])
    low = heapq.nsmallest(5, students, key = lambda s: s['avg'])

    print(

        "\n\nHihgest Avg: \n\n",
        '\n'.join(i['Name'] + ' ' + i['Surname'] + ' ' + str(i['avg']) \
        for i in high), "\n\n"

        "\nLowest Avg: \n\n",
        '\n'.join(i['Name'] + ' ' + i['Surname'] + ' ' + str(i['avg']) \
        for i in low),

        sep = ''
    )

    ### dictionaries ###

    a = {

        'a' : 2,
        'c' : 4
    }

    b = {

        'a' : 2,
        'd' : 8,
        'c' : 4
    }

    ## by keys ##
    print(

        "Common:\n\n",
        b.keys() & a.keys(),
        "\n\nDifference (b to a):\n\n",
        b.keys() - a.keys(), "\n"
    )

    ## by items ##
    print(

        "Common:\n\n",
        b.items() & a.items(),
        "\n\nDifference (b to a):\n\n",
        b.items() - a.items(),
    )

    ### get occurence ###

    numbers = [0, 1, 3, 1, 2, 0]
    numbers_cnt  = Counter(numbers)
    print(numbers_cnt.most_common(2), numbers_cnt[0])

    words = ['ala', 'has', 'a cat', 'and', 'the cat', 'has', 'ala']
    words_cnt = Counter(words)
    print(words_cnt.most_common(2))

    ### sorting dictionary ###

    students = [

        {'Name' : 'Anthony', 'Surname' : 'ABC', 'avg' : 3.5},
        {'Name' : 'Anthony', 'Surname' : 'ABD', 'avg' : 4.55},
        {'Name' : 'Anthony', 'Surname' : 'ABE', 'avg' : 4.6},
        {'Name' : 'Anthony', 'Surname' : 'ABA', 'avg' : 5.0},
        {'Name' : 'Anthony', 'Surname' : 'ABS', 'avg' : 4.7},
        {'Name' : 'Anthony', 'Surname' : 'ABY', 'avg' : 2.5},
        {'Name' : 'Anthony', 'Surname' : 'ABT', 'avg' : 4.8},
        {'Name' : 'Anthony', 'Surname' : 'ABM', 'avg' : 3.0},
        {'Name' : 'Anthony', 'Surname' : 'ABH', 'avg' : 3.7},
        {'Name' : 'Anthony', 'Surname' : 'ABF', 'avg' : 3.2}
    ]

    sorted_students = sorted(students, key = itemgetter('avg'))
    print("Sorted dict", sorted_students)

    sorted_students = sorted(students, key = lambda s: len(s['Surname']))
    print("Sorted dict with lamda", sorted_students)

    ### group by dict ###

    students = [

        {'Name' : 'Anthony', 'Surname' : 'ABC', 'avg' : 3.5},
        {'Name' : 'Anthony', 'Surname' : 'ABD', 'avg' : 4.55},
        {'Name' : 'Anthony', 'Surname' : 'ABE', 'avg' : 4.6},
        {'Name' : 'Anthony', 'Surname' : 'ABA', 'avg' : 5.0},
        {'Name' : 'Anthony', 'Surname' : 'ABS', 'avg' : 4.7},
        {'Name' : 'Anthony', 'Surname' : 'ABY', 'avg' : 2.5},
        {'Name' : 'Anthony', 'Surname' : 'ABT', 'avg' : 4.8},
        {'Name' : 'Anthony', 'Surname' : 'ABM', 'avg' : 3.0},
        {'Name' : 'Anthony', 'Surname' : 'ABH', 'avg' : 3.7},
        {'Name' : 'Anthony', 'Surname' : 'ABF', 'avg' : 3.2},
        {'Name' : 'Anthony', 'Surname' : 'ABYG', 'avg' : 3.2}
    ]

    students = sorted(students, key = lambda s: s['avg'])

    for field, group in groupby(students, key = itemgetter('avg')):

        for element in group:

            print('  ',element)

    ### find data in any dict ###

    dataset1 = {

        'car' : 'mercedes',
        'ala' : 'has a cat'
    }

    dataset2 = {

        'car' : 'bmw',
        'lynx' : 'has a cat'
    }

    print(ChainMap(dataset1, dataset2).get('car', False))

    #### wrapping text ####

    text = "Ala has a cat and the cat has a Lynx called XYZ"

    print(textwrap.fill(text, 20))

    ### wrap vs fill

    wraping = textwrap.wrap("Ala has a cat and the cat has Lynx", 15)

    filling = textwrap.fill("Ala has a cat and the cat has Lynx", 15)

    print("Wrap: \n", wraping, "\nNew line?", "\n" in wraping,
    "\nFill: \n", filling, "\nNew line?", "\n" in filling)

    ### date ###

    weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
    'Friday', 'Satruday', 'Sunday']

    for number, element in enumerate(weekdays):

        print("Number: ", number, " Element: ", element)

    ### chain ###
    st = time()

    weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
    'Friday', 'Satruday', 'Sunday']

    numbers = [i for i in range(1, 2000000)]

    letters = [i for i in "a b c d e f".split(" ")]

    for el in chain(letters, numbers, weekdays):

        a = 0

    print("Took: ", time()-st)

    ### normal way ###

    st = time()

    weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
    'Friday', 'Satruday', 'Sunday']

    weekdays.extend([i for i in range(1, 2000000)])

    weekdays.extend([i for i in "a b c d e f".split(" ")])

    for el in weekdays:

        a = 0

    print("Took for normal way: ", time()-st)


    ### io ###

    s = StringIO()
    s.write("Hello!\n")
    print("Bzdura", file = s)
    print(s.getvalue())

    ### csv ###

    dataset = [
        ['Number', 'Label', 'Value'],
        [1, 'Cat', 'someval'],
        [1, 'Cat', 'someval']
    ]

    dst_fname = "/tmp/plik.csv"

    ### create csv file ###
    if csv_write(dataset, dst_fname)['isOK']:

        print("File: ", dst_fname, " was created")

    else:

        print("File: ", dst_fname, " was NOT created")

    ### read csv file ###

    if csv_read(dst_fname)['isOK']:

        print("File: ", dst_fname, " was open succesfully",
        csv_read(dst_fname)['records'])

    else:

        print("File: ", dst_fname, " could not be opened")

    ### json ###

    #column names
    table_header = ['Name', 'Last Name', 'Age']

    #rows
    table_rows = [

        ['Ala', 'XYZ', 25],
        ['Anna', 'ABC', 45],
        ['Mark', 'Poi', 47]
    ]

    #create object of Pretty table with all borders
    pt = prettytable.PrettyTable(

        hrules = prettytable.ALL,
        header = True,
        padding_width = 3
    )

    #add column names
    pt.field_names = table_header

    for table_row in table_rows:

        #adding row
        pt.add_row(table_row)

    json_data = {

        'clients' : format(pt)
    }

    print(dumps(json_data))

    ### get diff ###

    slots = set(['sda', 'sdb', 'sdc'])

    new_slots = set(['sda', 'sdc', 'sdb', 'sdd'])

    print(new_slots - slots)

    #other

    help(int_only)

    cache = Cache (30)

    cache['moje'] = 2*2

    print(cache['moje'])

    sleep(35)
    cache['moje'] = 8
    print(cache['moje'])
